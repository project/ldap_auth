(function ($, Drupal) {
    Drupal.behaviors.filterError = {
        attach: function (context, settings) {

            let input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("search_ldap_search_base_field");

            input.addEventListener('keyup',function (){

                filter = input.value.toUpperCase();
                table = document.getElementById("search_base_table");
                tr = table.getElementsByTagName("tr");
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[1];
                    if (td) {
                        txtValue = td.textContent || td.innerText;
                        if (txtValue.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }

            });


        }
    };
})(jQuery, Drupal);