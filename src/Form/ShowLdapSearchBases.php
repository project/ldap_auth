<?php
namespace Drupal\ldap_auth\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\ldap_auth\LDAPFlow;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;


class ShowLdapSearchBases extends LDAPFormBase {

  public function getFormId() {
    return 'show_ldap_searchbases';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $ldap_server = $this->config->get('miniorange_ldap_server');

    $form['markup_library'] = [
        '#attached' => [
            'library' => [
                "ldap_auth/ldap_auth.livesearch",
            ],
        ],
    ];

    $form['#title'] = $this->t("LDAP server search bases list - $ldap_server");

    $form['markup'] = [
        '#markup' => "Please select the LDAP search bases under which you want to search your users."
    ];
    
    $form['search_ldap_search_base_field'] = [
      '#type' => 'search',
      '#id' => 'search_ldap_search_base_field',
      '#attributes' => [
        'placeholder' => 'Search for LDAP search base...',
      ],
    ];
    
    $default_value = $this->getDefaultValues();
    $rows = $this->getSearchBases();
    if ($default_value && isset($rows[$default_value])) {
      $rows = [$default_value => ['ldap_search_base' => $default_value]] + $rows;
    }
    
    $form['search_base_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'search_base_wrapper'],
    ];
    
    $form['search_base_wrapper']['search_base_table'] = [
      '#type' => 'tableselect',
      '#id' => 'search_base_table',
      '#empty' => $this->t('<b>No Search Base to show you. Please configure LDAP Binding.</b>'),
      '#header' => $this->getTableHeader(),
      '#options' => $rows,
      '#multiple' => false,
      '#default_value' => $default_value,
    ];
    $form['actions'] = ['#type' => 'actions'];
    
    $form['actions']['send'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save Search Bases'),
        '#button_type' => 'primary',
    ];
    
    $form['actions']['fetch_button'] = [
      '#type' => 'button',
      '#value' => 'Fetch Search Bases',
      '#ajax' => [
        'callback' => '::updateSearchBaseOptions',
        'wrapper' => 'search_base_wrapper',
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }
  
  private function getTableHeader(): array{
    return [
      'ldap_search_base' => $this->t("LDAP search base")
      ];
  }
  
  private function getDefaultValues(){
    return $this->config->get('miniorange_ldap_search_base') ?? "";
  }
  
  public function updateSearchBaseOptions(array &$form, FormStateInterface $form_state) {
    $ldapSearchBases = $this->getSearchBases(false);
    
    $form['search_base_wrapper']['search_base_table']['#options'] = $ldapSearchBases;
    \Drupal::messenger()->addMessage($this->t('Search bases fetched successfully.'));
    return $form['search_base_wrapper'];
  }
  
  /**
   * Fetches search bases on the given mode.
   *
   * @param bool $useSaved
   *   The mode of operation. Allowed values:
   *   - 'saved': Fetches only the saved search bases from configuration.
   *   - 'latest': Fetches only the latest search bases from the LDAP server.
   *
   * @return array|null
   *   An array of search bases or null if none are found.
   */
  private function getSearchBases(bool $useSaved = true): ?array
  {
    if(!$useSaved){
      $search_bases = (new LDAPFlow())->getSearchBases();
      return $this->generateSearchBaseOptions($search_bases);
    }
    
    $savedSearchBases = $this->config->get("possible_ldap_search_bases");
    $saved = $savedSearchBases ? json_decode($savedSearchBases, true) : (new LDAPFlow())->getSearchBases();
    
    return $this->generateSearchBaseOptions($saved);
  }
  
  /**
   * Generates the options array for search base table based on LDAP search bases.
   */
  private function generateSearchBaseOptions(array $ldapSearchBases = []) {
    $options = [];
    foreach ($ldapSearchBases as $ldapSearchBase) {
      $options[$ldapSearchBase] = [
        'ldap_search_base' => $ldapSearchBase
      ];
    }
    return $options;
  }
  
  
  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $searchBasesValues = $form_state->getValue('search_base_table');
    $this->config_factory->set("miniorange_ldap_search_base",$searchBasesValues)->save();
    $this->messenger->addMessage($this->t('Search bases has saved successfully.'),'status');
    return $this->redirect('ldap_auth.ldap_config', ['action' => 'edit'])->send();
  }
}